# Audio Spectrograph
Visualization for the jam room.

## Getting Started

### Installation
```
git clone git@gitlab.com:scienceworks-hands-on-museum/audio-spectrograph.git
cd audio-spectrograph

sudo apt update
sudo apt install virtualenv build-essential

# Make sure python3 is installed on your system:
python3 --version

virtualenv -p python3 env
. env/bin/activate

make install
```

### Run the webserver
```
# Make sure you have done . env/bin/activate first

make
```

Now you may open the page at http://127.0.0.1:50000