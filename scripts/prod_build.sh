#!/bin/bash
# Fast build for development.

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SLURP=$SCRIPTPATH/../src/JSUtils/Slurp/slurp/build
SRC=$1
DIST=$2
TMP=_DIST_TMP

rm -rf $TMP
mkdir -p $TMP

echo "Run Slurp pipeline."
$SLURP/slurpUp.py $SRC $DIST \
| $SLURP/ignore.py node_modules test tests scripts \
| $SLURP/spit.py --fileExtensions=png,jpg,jpeg,svg \
| $SLURP/fileLinker.py --fileExtensions=js,html,css \
| $SLURP/envVars.py --injectversion environment/prod.json \
| $SLURP/wrap.py "npx javascript-obfuscator [file] --compact true --self-defending false --output [file]" --fileExtensions=js --ignore=test,tests,whammy.js --only=apps/glitter \
| $SLURP/spit.py

# Treat stuff inside <script> tags as files.
echo "Parse JS inside <script> tags."
$SLURP/scriptTagsToTempFiles.py $DIST $TMP

echo "Run babel."
npx babel --minified $TMP -d $TMP

# Link files back inside of <script> tags.
echo "Link files back into <script> tags."
$SLURP/slurpUp.py $DIST $DIST \
| $SLURP/ignore.py node_modules test tests \
| $SLURP/fileLinker.py --fileExtensions=js,htm,html \
| $SLURP/spit.py

# Render static pages.
echo "Render static pages."
$SLURP/staticify.py \
    --sourceFilePaths="$3" \
    --destinationFilePaths="$4" \
    --webdriverLocation="~/chromedriver" \
    --flagName="PAGE_RENDERED_STATIC"

rm -rf $TMP
