import('jsgame/Game.js');
import('jsgame/props/ImageProp.js');

/**
 * @class
 */
class VisualizationGame extends Game {

    /**
     * @public
     * @override
     */
    static init() {
        super.init(...arguments);

        const audioCtx = new (window.AudioContext || window.webkitAudioContext)();

        if (navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({"audio": true}).then((stream) => {
                const microphone = audioCtx.createMediaStreamSource(stream);

                this.analyzer = audioCtx.createAnalyser();
                this.analyzer.fftSize = 2048;

                this.bufferLength = this.analyzer.frequencyBinCount;
                this.freqRangeSize = (audioCtx.sampleRate / 2) / this.analyzer.frequencyBinCount;

                this.dataArray = new Uint8Array(this.bufferLength);

                this.analyzer.getByteTimeDomainData(this.dataArray);

                microphone.connect(this.analyzer);

            }).catch((err) => {
                console.warn('No microphone access');
            });
        } else {
            console.warn('Could not access media devices');
            console.warn('Are you in a secure context?');
        }

        this.bgImage = new ImageProp(0, 0, this._BG_URI);
        this.bgImage.load();
        this.currentRoom.addProp(this.bgImage);
        this._effectScale = 1.0;
        this._runningAverageData = [];
        this._sampleRate = audioCtx.sampleRate;

        this.resize();
    }

    /**
     * @private
     */
    static getAmplitudeRelativeToRunningAverage(bufferLength) {
        const avg = this.dataArray.slice(0, bufferLength).reduce((x, acc) => x + acc) / bufferLength;

        this._runningAverageData.push(avg);
        this._runningAverageData = this._runningAverageData.slice(0, this._RUNNING_AVERAGE_N_FRAMES);

        const runningAverage = this._runningAverageData.reduce((x, acc) => x + acc) / this._runningAverageData.length;

        return avg - runningAverage;
    }

    /**
     * @private
     */
    static _getHz(i) {
        return Math.round(0.5 * this._sampleRate * (i / this.bufferLength));
    }
    
    /**
     * @public
     * @override
     */
    static draw() {
        this.graphics.background(0);

        super.draw();

        if (!this.analyzer) {
            console.warn('this.analyzer undefined');
            return;
        }


        // Read from mic.
        this.analyzer.getByteFrequencyData(this.dataArray);

        const bufferLength = 64;
        const avg = this.getAmplitudeRelativeToRunningAverage(bufferLength);

        // Scale img.
        this._effectScale += avg / 200;
        this._effectScale *= 0.8;
        this._effectScale = Math.max(this._effectScale, 0.5);
        this._effectScale = Math.min(this._effectScale, 2.0);


        const imgScale = 1 / (this._effectScale * 2);
        this.bgImage.setScale(imgScale);
        this.bgImage.x = this.viewWidth / 2 - (this.bgImage.width * imgScale) / 2;
        this.bgImage.y = this.viewHeight / 2 - (this.bgImage.height * imgScale) / 2;      

        // Spectrograph.
        const sliceWidth = Math.ceil(this.viewWidth / bufferLength);
        let x = 0;

        for (let i = 0; i < bufferLength; i++) {
            const v = this.dataArray[i] / 128.0;
            const y = (v * this.viewHeight) / 2;

            const h = 360 * (i / (bufferLength - 1));
            this.graphics.setFillStyle('hsla(' + h + ', 61%, 34%, 0.9');
            this.graphics.rect(x, this.viewHeight - y, sliceWidth, y);

            x += sliceWidth;
        }

        // Text.
        this.graphics.textAlign(Shapes.ALIGN.CENTER);
        this.graphics.textSize(20 + this._effectScale * 20);

        this.graphics.fill(0);
        this.graphics.text('Welcome to THE JAM', this.viewWidth / 2 + 2, 64 + 2);
        this.graphics.fill(255);
        this.graphics.text('Welcome to THE JAM', this.viewWidth / 2, 64);
        


        // Frequency labels.
        this.graphics.fill(255);
        this.graphics.textSize(24);
        x = 0;

        for (let i = 0; i < bufferLength; i++) {

            if (i > 0 && i < bufferLength - 1 && i % 5 === 0) {
                const hz = this._getHz(i);
                
                this.graphics.text(hz + 'Hz', x + sliceWidth / 2, this.viewHeight - 32);
            }

            x += sliceWidth;
        }
    }

    /**
     * @public
     * @override
     */
    static resize() {
        const pageSize = Page.getSize();
        this.viewWidth = pageSize.width;
        this.viewHeight = pageSize.height;
    }
}

VisualizationGame._BG_URI = 'background.jpg';
VisualizationGame._RUNNING_AVERAGE_N_FRAMES = 100;