import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('VisualizationGame.js');


/**
 * @class
 */
class HomepageViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        this._started = false;
        this.game = VisualizationGame;
    }

    /**
     * @public
     */
    static start() {
        if (this._started) return;

        this._started = true;
        Dom.removeElement(ID.START_BUTTON);

        this.game.init(ID.CANVAS_CONTAINER);
    }

}

Page.addLoadEvent(() => {
    HomepageViewDriver.init();

    VisualizationGame.resize();

});

Page.addResizeEvent(() => {
    VisualizationGame.resize();
});

const ID = {
    CANVAS_CONTAINER: 'container',
    START_BUTTON: 'start',
    TEMPLATE: {
        
    }
};

const CLASS = {

};
